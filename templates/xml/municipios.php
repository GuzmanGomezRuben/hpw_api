<?xml version="1.0" encoding="UTF-8"?>
<municipios>
<?php foreach ($municipios as $municipio) { ?>
  <municipio>
    <id_entidad><?php echo $municipio['id_entidad']; ?></id_entidad>
    <id_municipio><?php echo $municipio['id_municipio']; ?></id_municipio>
    <nombre_municipio><?php echo $municipio['nombre_municipio']; ?></nombre_municipio>
  </municipio>
<?php } ?>
</municipios>
