<?xml version="1.0" encoding="UTF-8"?>
<servicios>
<?php foreach ($servicios as $Servicio) { ?>
  <servicio>
    <id_servicio><?php echo $Servicio['id_servicio'];?></id_servicio>
	<nombre_servicio><?php echo $Servicio['nombre_unidad_actividad']?></nombre_servicio>
	<razon_social><?php echo $Servicio['razon_social'];?></razon_social>
    <nombre_clase_actividad><?php echo $Servicio['nombre_clase_actividad'];?></nombre_clase_actividad>
	<descripcion_estrato><?php echo $Servicio['descripcion_estrato'];?></descripcion_estrato>
	<tipo_centro_comercial><?php echo $Servicio['tipo_centro_comercial'];?></tipo_centro_comercial>
	<tipo_establecimiento><?php echo $Servicio['tipo_establecimiento'];?></tipo_establecimiento>
	<id_entidad><?php echo $Servicio['clave_entidad'];?></id_entidad>
	<id_municipio><?php echo $Servicio['clave_municipio'];?></id_municipio>
	<id_localidad><?php echo $Servicio['clave_localidad'];?></id_localidad>
  </servicio>
<?php } ?>
</servicios>
