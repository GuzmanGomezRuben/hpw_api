<?xml version="1.0" encoding="UTF-8"?>
<servicios>
<?php foreach ($serviciosDireccion as $Servicio) { ?>
  <servicio>
	<nombre_tipo_vialidad><?php echo $Servicio['t'];?></nombre_tipo_vialidad>
	<nombre_vialidad><?php echo $Servicio['nombre_vialidad'];?></nombre_vialidad>
	<nombre_tipo_vialidad1><?php echo $Servicio['t1'];?></nombre_tipo_vialidad1>
	<nombre_vialidad1><?php echo $Servicio['nombre_vialidad1'];?></nombre_vialidad1>
	<nombre_tipo_vialidad2><?php echo $Servicio['t2'];?></nombre_tipo_vialidad2>
	<nombre_vialidad2><?php echo $Servicio['nombre_vialidad2'];?></nombre_vialidad2>
	<nombre_tipo_vialidad3><?php echo $Servicio['t3'];?></nombre_tipo_vialidad3>
	<nombre_vialidad3><?php echo $Servicio['nombre_vialidad3'];?></nombre_vialidad3>
	<numero_exterior_km><?php echo $Servicio['numero_exterior_km'];?></numero_exterior_km>
	<letra_exterior><?php echo $Servicio['letra_exterior'];?></letra_exterior>
	<edificio><?php echo $Servicio['edificio'];?></edificio>
	<edificio_piso><?php echo $Servicio['edificio_pis'];?></edificio_piso>
	<numero_interior><?php echo $Servicio['numero_interior'];?></numero_interior>
	<letra_interior><?php echo $Servicio['letra_interior'];?></letra_interior>
	<tipo_asentamiento_hum><?php echo $Servicio['tipo_asentamiento_hum'];?></tipo_asentamiento_hum>			
	<corredor_industrial><?php echo $Servicio['corredor_industrial'];?></corredor_industrial>
	<numero_local><?php echo $Servicio['numero_local'];?></numero_local>
	<cp><?php echo $Servicio['cp'];?></cp>		
	<area_geoestadistica><?php echo $Servicio['area_geoestadistica'];?></area_geoestadistica>
	<manzana><?php echo $Servicio['manzana'];?></manzana>
  </servicio>
<?php } ?>
</servicios>
