<?xml version="1.0" encoding="UTF-8"?>
<localidades>
<?php foreach ($localidades as $localidad) { ?>
  <localidad>
    <id_entidad><?php echo $localidad['id_entidad']; ?></id_entidad>
    <id_municipio><?php echo $localidad['id_municipio']; ?></id_municipio>
	<id_localidad><?php echo $localidad['id_localidad']; ?></id_localidad>
    <nombre_localidad><?php echo $localidad['nombre_localidad']; ?></nombre_localidad>
  </localidad>
<?php } ?>
</localidades>
