{
	"servicios":{
		"servicio":[
		<?php
			$total = count($servicios);
			$ultimo = $total - 1;
			$contador = 0;
			foreach($servicios as $Servicio){
				
?>
		{
			"id_servicio": <?php echo $Servicio['id_servicio'];?>,
			"nombre_servicio": "<?php echo $Servicio['nombre_unidad_actividad']?>",
			"razon_social": "<?php echo $Servicio['razon_social'];?>",
            "nombre_clase_actividad": "<?php echo $Servicio['nombre_clase_actividad'];?>",
			"descripcion_estrato": "<?php echo $Servicio['descripcion_estrato'];?>",
			"tipo_centro_comercial": "<?php echo $Servicio['tipo_centro_comercial'];?>",
			"tipo_establecimiento": "<?php echo $Servicio['tipo_establecimiento'];?>",
			"id_entidad": <?php echo $Servicio['clave_entidad'];?>,
			"id_municipio": <?php echo $Servicio['clave_municipio'];?>,
			"id_localidad": <?php echo $Servicio['clave_localidad'];?>

<?php
	if($contador !== $ultimo){
		$fin = '},';
		$contador ++;
	}else{
		$fin='}';
	}
?>
<?php	echo $fin; ?>

<?php } ?>
		]
	
}
}
