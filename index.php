<?php
require 'vendor/autoload.php';
$app = new \Slim\Slim();

define('BD_SERVIDOR', '127.0.0.1');
define('BD_NOMBRE', 'Servicios'); //SERV_MOD
define('BD_USUARIO', 'rube');
define('BD_PASSWORD', 'kjKszpj');
define('DB_PORT', '5432');

$db = new PDO("pgsql:host=" . BD_SERVIDOR . ";dbname=" . BD_NOMBRE . ";port=" . DB_PORT, BD_USUARIO, BD_PASSWORD);

/////////////////////////////////////////////////////////
//ENTIDADES
$app->get('/entidades', function() use ($db, $app) {
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml'");
    }
    try {
        $consulta = $db->prepare("select * from \"Entidad\"");
        $consulta->execute();
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/entidades.php', array(
            'entidades' => $resultados
        ));
    }else if($tipo_contenido === 'application/xml'){
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/entidades.php', array(
            'entidades' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//GET ENTIDAD
$app->get('/entidades/:id', function($id) use ($db, $app) {
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json','application/xml'");
    }

    try {
        $consulta = $db->prepare("select * from \"Entidad\" where id_entidad = :id");
        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Entidad no encontrada.");
    }

    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/entidades.php', array(
            'entidades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/entidades.php', array(
            'entidades' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//TODOS LOS MUNICIPIOS DE UNA ENTIDAD
$app->get('/entidades/:id/municipios', function($id) use ($db, $app) {
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json','application/xml'");
    }

    try {
        $consulta = $db->prepare("select * from \"Municipio\" where id_entidad = :id");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if (count($resultados) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Municipios no encontrados.");
    }

    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/municipios.php', array(
            'municipios' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
    $app->response->headers->set('Content-Type', 'application/xml');
    $app->render('xml/municipios.php', array(
        'municipios' => $resultados
    ));
    }
});

/////////////////////////////////////////////////////////
//MUNICIPIO DE UNA ENTIDAD
$app->get('/entidades/:id_entidad/municipios/:id_municipio', function($id_entidad, $id_municipio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml'");
    }
    try {
        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Municipio\" 
        where 
            id_entidad = :id_entidad and id_municipio = :id_municipio 
        order by 
            id_municipio ASC");

        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Municipio no encontrado.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/municipios.php', array(
            'municipios' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
    $app->response->headers->set('Content-Type', 'application/xml');
    $app->render('xml/municipios.php', array(
        'municipios' => $resultados
    ));
}
});

/////////////////////////////////////////////////////////
//ELIMINAR MUNICIPIO DE UNA ENTIDAD
$app->delete('/entidades/:id_entidad/municipios/:id_municipio', 
    function($id_entidad, $id_municipio) use ($db, $app) {
    
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml' ) {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json','application/xml'");
    }

    try {

        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Municipio\" 
        where 
            id_entidad = :id_entidad and id_municipio = :id_municipio"
        );

        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio
        ));

        $resultados = $consulta->fetchAll();

        if (count($resultados) == 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Municipio no encontrado.");
        }

        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Localidad\" 
        where 
            id_entidad = :id_entidad and id_municipio = :id_municipio"
        );

        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio
        ));

        $resultados = $consulta->fetchAll();

        if (count($resultados) != 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(409, "Este municipio contiene datos");
        } else {

            $consulta = $db->prepare("
            delete 
            from 
                \"Municipio\" 
            where 
                id_entidad = :id_entidad and id_municipio = :id_municipio");

            $consulta->execute(array(
                ':id_entidad' => $id_entidad,
                ':id_municipio' => $id_municipio
            ));
            $resultados = $consulta->fetchAll();

            $app->halt(200, 'Municipio borrado');
        }
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
});

/////////////////////////////////////////////////////////
//CREAR MUNICIPIO
$app->post('/entidades/:id_entidad/municipios', 
    function($id_entidad) use ($db, $app) {
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {
        /* Procesar JSON */
        $municipio = @json_decode($cuerpo_solicitud)->municipios->municipio;
        if (is_null($municipio) || json_last_error() !== JSON_ERROR_NONE || count($municipio) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nombre_nuevo_municipio = $municipio[0]->nombre_municipio;
    }else  if ($tipo_contenido === 'application/xml; charset=UTF-8') {
          /* Procesar XML */
        $municipio = @simplexml_load_string($cuerpo_solicitud)->municipio;
        if (empty($municipio) || count($municipio) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }        
        $nombre_nuevo_municipio = $municipio[0]->nombre_municipio;
    }


    $consulta = $db->prepare("
    insert into
    \"Municipio\" (id_entidad,nombre_municipio)
    values (:id_entidad,:nombre)
  ");
    $consulta->execute(array(
        ':id_entidad' => $id_entidad,
        ':nombre' => $nombre_nuevo_municipio
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Municipio ya existente.");
    }

    $id_nuevo_grupo = $db->lastInsertId();
    $app->response->headers->set('Location', '/grupos/' . $id_nuevo_grupo);
    $app->halt(201, 'El Municipio ha sido creado.');
});

/////////////////////////////////////////////////////////
//ACTUALIZAR MUNICIPIO
$app->put('/entidades/:id_entidad/municipios/:id_municipio', 
        function($id_entidad, $id_municipio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {
        /* Procesar JSON */
        $municipio = @json_decode($cuerpo_solicitud)->municipios->municipio;
        if (is_null($municipio) || json_last_error() !== JSON_ERROR_NONE || count($municipio) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nombre_nuevo_municipio = $municipio[0]->nombre_municipio;
    }
     if ($tipo_contenido === 'application/xml; charset=UTF-8') {
           /* Procesar XML */
        $municipio = @simplexml_load_string($cuerpo_solicitud)->municipio;
        if (empty($municipio) || count($municipio) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }        
        $nombre_nuevo_municipio = $municipio[0]->nombre_municipio;
     }

    $consulta = $db->prepare("
    select 
        *
    from 
        \"Municipio\"
    where 
        id_entidad = :id_entidad and id_municipio = :id_municipio
  ");
    $consulta->execute(array(
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio
    ));
    $resultado = $consulta->fetchAll();
    if (count($resultado) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Municipio no encontrado.");
    }
    $consulta = $db->prepare("
    update
        \"Municipio\"
    set 
        nombre_municipio = :nombre_municipio
    where 
        id_entidad=:id_entidad and id_municipio=:id_municipio
    ");
    $consulta->execute(array(
        ':nombre_municipio' => $nombre_nuevo_municipio,
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Nombre de municipio ya existente.");
    }

    $app->halt(200, 'Municipio actualizado.');
});

/////////////////////////////////////////////////////////
//LOCALIDADES DE UN MUNICIPIO PERTENECIENTES A UNA ENTIDAD
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades',
    function($id_entidad, $id_municipio) use ($db, $app) {

    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml' ) {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json' , 'application/xml'");
    }
    try {
        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Localidad\" 
        where 
            id_entidad = :id_entidad and id_municipio = :id_municipio");
        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/localidades.php', array(
            'localidades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/localidades.php', array(
            'localidades' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//LOCALIDAD DE UN MUNICIPIO PERTENECIENTE A UNA ENTIDAD
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad',
    function($id_entidad, $id_municipio, $id_localidad) use ($db, $app) {

    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json'");
    }
    try {
        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Localidad\" 
        where 
            id_entidad = :id_entidad and 
            id_municipio = :id_municipio and 
            id_localidad = :id_localidad");
        
        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio,
            ':id_localidad' => $id_localidad
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if (count($resultados) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Localidad no encontrada.");
    }

    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/localidades.php', array(
            'localidades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
    $app->response->headers->set('Content-Type', 'application/xml');
    $app->render('xml/localidades.php', array(
        'localidades' => $resultados
    ));
    }
});

/////////////////////////////////////////////////////////
//CREAR LOCALIDAD
$app->post('/entidades/:id_entidad/municipios/:id_municipio/localidades', 
    function($id_entidad, $id_municipio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {
        /* Procesar JSON */
        $localidad = @json_decode($cuerpo_solicitud)->localidades->localidad;
        if (is_null($localidad) || json_last_error() !== JSON_ERROR_NONE || count($localidad) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nombre_nuevo_localidad = $localidad[0]->nombre_localidad;
    }else  if ($tipo_contenido === 'application/xml; charset=UTF-8') {
          /* Procesar XML */
        $localidad = @simplexml_load_string($cuerpo_solicitud)->localidad;
        if (empty($localidad) || count($localidad) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }        
        $nombre_nuevo_localidad = $localidad[0]->nombre_localidad;
    }

    $consulta = $db->prepare("
    insert into
    \"Localidad\" (id_entidad,id_municipio,nombre_localidad)	
    values (:id_entidad,:id_municipio,:nombre)
  ");
    $consulta->execute(array(
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio,
        ':nombre' => $nombre_nuevo_localidad
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Localidad ya existe.");
    }

    $id_nuevo_grupo = $db->lastInsertId();
    $app->response->headers->set('Location', '/entidad/' . $id_entidad . '/municipios/' . $id_municipio . '/localidad/' . $id_nuevo_grupo);
    $app->halt(201, 'La localidad ha sido creada.');
});

/////////////////////////////////////////////////////////
//ELIMINAR LOCALIDAD
$app->delete('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad', 
    function($id_entidad, $id_municipio, $id_localidad) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml");
    }

    try {

        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Localidad\" 
        where 
            id_entidad = :id_entidad and 
            id_municipio = :id_municipio and 
            id_localidad = :id_localidad"
        );
        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio,
            ':id_localidad' => $id_localidad
        ));

        $resultados = $consulta->fetchAll();
        if (count($resultados) == 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Localidad no encontrada.");
        }

        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Servicio\" 
        where 
            id_entidad = :id_entidad and 
            id_municipio = :id_municipio and 
            id_localidad = :id_localidad"
        );

        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio,
            ':id_localidad' => $id_localidad
        ));

        $resultados = $consulta->fetchAll();

        if (count($resultados) != 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(409, "La localidad contiene datos");
        } else {

            $consulta = $db->prepare("
            delete from 
                \"Localidad\" 
            where 
                id_entidad = :id_entidad and 
                id_municipio = :id_municipio and 
                id_localidad = :id_localidad"
            );

            $consulta->execute(array(
                ':id_entidad' => $id_entidad,
                ':id_municipio' => $id_municipio,
                ':id_localidad' => $id_localidad
            ));
            $resultados = $consulta->fetchAll();

            $app->halt(200, 'Localidad borrada');
        }
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
});

/////////////////////////////////////////////////////////
//ACTUALIZAR LOCALIDAD
$app->put('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad', 
    function($id_entidad, $id_municipio, $id_localidad) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    $tipo_contenido = $app->request->headers->get('Content-Type');
    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {
        /* Procesar JSON */
        $localidad = @json_decode($cuerpo_solicitud)->localidades->localidad;
        if (is_null($localidad) || json_last_error() !== JSON_ERROR_NONE || count($localidad) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nombre_nuevo_localidad = $localidad[0]->nombre_localidad;
    }else  if ($tipo_contenido === 'application/xml; charset=UTF-8') {
          /* Procesar XML */
        $localidad = @simplexml_load_string($cuerpo_solicitud)->localidad;
        if (empty($localidad) || count($localidad) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }        
        $nombre_nuevo_localidad = $localidad[0]->nombre_localidad;
    }

    $consulta = $db->prepare("
    select 
        *
    from 
        \"Localidad\"
    where 
        id_entidad = :id_entidad and 
        id_municipio = :id_municipio and 
        id_localidad = :id_localidad
  ");
    $consulta->execute(array(
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio,
        ':id_localidad' => $id_localidad
    ));
    $resultado = $consulta->fetchAll();
    if (count($resultado) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Localidad no encontrada.");
    }

    $consulta = $db->prepare("
    update
        \"Localidad\"
    set 
        nombre_localidad = :nombre_localidad
    where 
        id_entidad=:id_entidad and 
        id_municipio=:id_municipio and 
        id_localidad = :id_localidad
  ");
    $consulta->execute(array(
        ':nombre_localidad' => $nombre_nuevo_localidad,
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio,
        ':id_localidad' => $id_localidad
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Nombre de la localidad  ya existe.");
    }

    $app->halt(200, 'Localidad actualizada.');
});

/////////////////////////////////////////////////////////
//GET SERVICIOS
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios', 
        function($id_entidad, $id_municipio, $id_localidad) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml'");
    }
    try {

        $consulta = $db->prepare(" 
        SELECT
            id_servicio,
            nombre_unidad_actividad,
            razon_social,
            ac.nombre_clase_actividad,
            d.descripcion_estrato,					
            tipo_centro_comercial,
            tipo_establecimiento,
            clave_entidad,
            clave_municipio,
            clave_localidad
        FROM
            \"Servicio\" AS s,
            \"Descripcion_estrato_personal\" AS d,
            \"Actividad\" AS ac
        WHERE
            s.descripcion_estrato = d.id_descripcion
            AND s.codigo_actividad = ac.codigo_actividad

            AND s.clave_entidad = :id_entidad
            AND s.clave_municipio = :id_municipio
            AND s.clave_localidad = :id_localidad
        ORDER BY id_servicio ASC
        ");
        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio,
            ':id_localidad' => $id_localidad
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    
    if (count($resultados)  === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Servicio no encontrado.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/servicios.php', array(
            'servicios' => $resultados
        ));
    }else  if ($tipo_contenido === 'application/xml') {
    $app->response->headers->set('Content-Type', 'application/xml');
    $app->render('xml/servicios.php', array(
        'servicios' => $resultados
    ));
    }
});

/////////////////////////////////////////////////////////
// GET SERVICIO
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id_servicio', 
    function($id_entidad, $id_municipio, $id_localidad, $id_servicio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    if (!is_numeric($id_servicio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json'");
    }
    try {

        $consulta = $db->prepare(" 
        SELECT
                id_servicio,
                nombre_unidad_actividad,
                razon_social,
                ac.nombre_clase_actividad,
                d.descripcion_estrato,					
                tipo_centro_comercial,
                tipo_establecimiento,
                clave_entidad,
                clave_municipio,
                clave_localidad
        FROM
                \"Servicio\" AS s,
                \"Descripcion_estrato_personal\" AS d,
                \"Actividad\" AS ac
        WHERE
                s.descripcion_estrato = d.id_descripcion
                AND s.codigo_actividad = ac.codigo_actividad

                AND s.clave_entidad = :id_entidad
                AND s.clave_municipio = :id_municipio
                AND s.clave_localidad = :id_localidad
                AND s.id_servicio = :id_servicio
        ");
        $consulta->execute(array(
            ':id_entidad' => $id_entidad,
            ':id_municipio' => $id_municipio,
            ':id_localidad' => $id_localidad,
            ':id_servicio' => $id_servicio
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if (count($resultados)  === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Servicio no encontrado.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/servicios.php', array(
            'servicios' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/servicios.php', array(
            'servicios' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//CREAR SERVICIO
$app->post('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios', 
    function($id_entidad, $id_municipio, $id_localidad) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'".$tipo_contenido);
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8' ) {
        /* Procesar JSON */
        $servicio = @json_decode($cuerpo_solicitud)->servicios->servicio;
        if (is_null($servicio) || json_last_error() !== JSON_ERROR_NONE || count($servicio) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_nombre_servicio = $servicio[0]->nombre_servicio;
        $nuevo_razon_social = $servicio[0]->razon_social;
        $nuevo_actividad = $servicio[0]->nombre_clase_actividad;
        $nuevo_descripcion_estrato = $servicio[0]->descripcion_estrato;
        $nuevo_tipo_establecimiento = $servicio[0]->tipo_establecimiento;
        $nuevo_id_entidad = $servicio[0]->id_entidad;
        $nuevo_id_municipio = $servicio[0]->id_municipio;
        $nuevo_id_localidad = $servicio[0]->id_localidad;
        $nuevo_tipo_centro_comercial = $servicio[0]->tipo_centro_comercial;

        if (is_null($nuevo_nombre_servicio) || is_null($nuevo_actividad)) {
            $app->halt(406, 'nombre_unidad_actividad y actividad no pueden ser vacios');
        }
    }else if($tipo_contenido === 'application/xml; charset=UTF-8'){
          /* Procesar XML */
        $servicio = @simplexml_load_string($cuerpo_solicitud)->servicio;
        if (empty($servicio) || count($servicio) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }
        $nuevo_nombre_servicio = $servicio[0]->nombre_servicio;
        $nuevo_razon_social = $servicio[0]->razon_social;
        $nuevo_actividad = $servicio[0]->nombre_clase_actividad;
        $nuevo_descripcion_estrato = $servicio[0]->descripcion_estrato;
        $nuevo_tipo_establecimiento = $servicio[0]->tipo_establecimiento;
        $nuevo_id_entidad = $servicio[0]->id_entidad;
        $nuevo_id_municipio = $servicio[0]->id_municipio;
        $nuevo_id_localidad = $servicio[0]->id_localidad;
        $nuevo_tipo_centro_comercial = $servicio[0]->tipo_centro_comercial;

        if (is_null($nuevo_nombre_servicio) || is_null($nuevo_actividad)) {
            $app->halt(406, 'nombre_servicio y nombre_clase_actividad no pueden ser vacios');
        }       
    }

    $consulta = $db->prepare("
    select 
        codigo_actividad 
    from 
        \"Actividad\" 
    where 
        nombre_clase_actividad like :nuevo_actividad");
    $consulta->execute(array(
        ':nuevo_actividad' => $nuevo_actividad
    ));
    $resultados = $consulta->fetchAll();
    if (count($resultados) === 0) {
        $app->halt(406, 'Nombre de la actividad invalido');
    }
    $codigo_actividad = $resultados[0]['codigo_actividad'];


    $consulta = $db->prepare("
    select 
        id_descripcion 
    from 
        \"Descripcion_estrato_personal\" 
    where 
        descripcion_estrato like :descripcion_estrato");

    $consulta->execute(array(
        ':descripcion_estrato' => $nuevo_descripcion_estrato
    ));
    $resultados = $consulta->fetchAll();
    if (count($resultados) === 0) {
        $app->halt(406, 'Descripcion estrato invalido');
    }
    $id_estrato = $resultados[0]['id_descripcion'];


    $consulta = $db->prepare("
    insert into
        \"Servicio\" 
        (nombre_unidad_actividad,
        razon_social,
        codigo_actividad,
        descripcion_estrato,
        tipo_centro_comercial,
        tipo_establecimiento,
        clave_entidad,
        clave_municipio,
        clave_localidad)	
    values 
        (:nombre_unidad_actividad,
        :razon_social,
        :codigo_actividad,
        :descripcion_estrato,
        :tipo_centro_comercial,
        :tipo_establecimiento, 
        :id_entidad, 
        :id_municipio, 
        :id_localidad);
    ");
    
    $consulta->execute(array(
        ':nombre_unidad_actividad' => $nuevo_nombre_servicio,
        ':razon_social' => $nuevo_razon_social,
        ':codigo_actividad' => $codigo_actividad,
        ':descripcion_estrato' => $id_estrato,
        ':tipo_centro_comercial' => $nuevo_tipo_centro_comercial,
        ':tipo_establecimiento' => $nuevo_tipo_establecimiento,
        ':id_entidad' => $id_entidad,
        ':id_municipio' => $id_municipio,
        ':id_localidad' => $id_localidad
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Servicio ya existe.");
    }

    $id_nuevo_grupo = $db->lastInsertId();
    $app->response->headers->set('Location', '/entidad/' . $id_entidad . '/municipios/' . $id_municipio . '/localidad/' . $id_localidad . '/servicios' . $id_nuevo_grupo);

    $app->halt(201, 'El servicio ha ido creado.');
});

//ACTUALIZAR SERVICIO
$app->put('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id_servicio', 
    function($id_entidad, $id_municipio, $id_localidad, $id_servicio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_servicio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {
        /* Procesar JSON */
        $servicio = @json_decode($cuerpo_solicitud)->servicios->servicio;
        if (is_null($servicio) || json_last_error() !== JSON_ERROR_NONE || count($servicio) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_id_servicio = $servicio[0]->id_servicio;
        $nuevo_nombre_servicio = $servicio[0]->nombre_servicio;
        $nuevo_razon_social = $servicio[0]->razon_social;
        $nuevo_actividad = $servicio[0]->nombre_clase_actividad;
        $nuevo_descripcion_estrato = $servicio[0]->descripcion_estrato;
        $nuevo_tipo_establecimiento = $servicio[0]->tipo_establecimiento;
        $nuevo_id_entidad = $servicio[0]->id_entidad;
        $nuevo_id_municipio = $servicio[0]->id_municipio;
        $nuevo_id_localidad = $servicio[0]->id_localidad;
        $nuevo_tipo_centro_comercial = $servicio[0]->tipo_centro_comercial;

        if (is_null($nuevo_nombre_servicio) || is_null($nuevo_actividad)) {
            $app->halt(406, 'nombre_unidad_actividad y actividad no pueden ser vacios');
        }
    }else if($tipo_contenido === 'application/xml; charset=UTF-8'){
          /* Procesar XML */
        $servicio = @simplexml_load_string($cuerpo_solicitud)->servicio;
        if (empty($servicio) || count($servicio) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }
        $nuevo_nombre_servicio = $servicio[0]->nombre_servicio;
        $nuevo_razon_social = $servicio[0]->razon_social;
        $nuevo_actividad = $servicio[0]->nombre_clase_actividad;
        $nuevo_descripcion_estrato = $servicio[0]->descripcion_estrato;
        $nuevo_tipo_establecimiento = $servicio[0]->tipo_establecimiento;
        $nuevo_id_entidad = $servicio[0]->id_entidad;
        $nuevo_id_municipio = $servicio[0]->id_municipio;
        $nuevo_id_localidad = $servicio[0]->id_localidad;
        $nuevo_tipo_centro_comercial = $servicio[0]->tipo_centro_comercial;

        if (is_null($nuevo_nombre_servicio) || is_null($nuevo_actividad)) {
            $app->halt(406, 'nombre_servicio y nombre_clase_actividad no pueden ser vacios');
        }       
    }

    $consulta = $db->prepare("
    select 
        codigo_actividad 
    from 
        \"Actividad\" 
    where 
        nombre_clase_actividad like :nuevo_actividad"
    );
    $consulta->execute(array(
        ':nuevo_actividad' => $nuevo_actividad
    ));
    $resultados = $consulta->fetchAll();
    if (count($resultados) === 0) {
        $app->halt(406, 'Nombre de la actividad invalido');
    }
    $codigo_actividad = $resultados[0]['codigo_actividad'];

    $consulta = $db->prepare("
    select 
        id_descripcion 
    from 
        \"Descripcion_estrato_personal\" 
    where 
        descripcion_estrato like :descripcion_estrato");

    $consulta->execute(array(
        ':descripcion_estrato' => $nuevo_descripcion_estrato
    ));
    $resultados = $consulta->fetchAll();
    if (count($resultados) === 0) {
        $app->halt(406, 'Descripcion estrato invalido');
    }
    $id_estrato = $resultados[0]['id_descripcion'];

    $consulta = $db->prepare("select * from \"Servicio\" where id_servicio=:id_servicio");
    $consulta->execute(array(
        ':id_servicio' => $id_servicio
    ));


    $consulta = $db->prepare("
    UPDATE 
        \"Servicio\"
    SET
        nombre_unidad_actividad = :nombre_unidad_actividad,
        razon_social = :razon_social,
        codigo_actividad = :codigo_actividad,
        descripcion_estrato = :descripcion_estrato,
        tipo_centro_comercial = :tipo_centro_comercial,
        tipo_establecimiento = :tipo_establecimiento,
        clave_entidad = :id_entidad,
        clave_municipio = :id_municipio,
        clave_localidad = :id_localidad
    WHERE	
        id_servicio = :id_servicio
  ");

    $consulta->execute(array(
        ':nombre_unidad_actividad' => $nuevo_nombre_servicio,
        ':razon_social' => $nuevo_razon_social,
        ':codigo_actividad' => $codigo_actividad,
        ':descripcion_estrato' => $id_estrato,
        ':tipo_centro_comercial' => $nuevo_tipo_centro_comercial,
        ':tipo_establecimiento' => $nuevo_tipo_establecimiento,
        ':id_entidad' => $nuevo_id_entidad,
        ':id_municipio' => $nuevo_id_municipio,
        ':id_localidad' => $nuevo_id_localidad,
        ':id_servicio' => $id_servicio
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Servicio ya existe.");
    }


    $app->halt(200, 'El servicio ha sido actualizado.');
});

/////////////////////////////////////////////////////////
//DELETE SERVICIO
$app->delete('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id_servicio', 
    function($id_entidad, $id_municipio, $id_localidad, $id_servicio) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    if (!is_numeric($id_servicio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json'");
    }
    try {

        $consulta = $db->prepare("Select * from \"Servicio\" where id_servicio = :id_servicio");
        $consulta->execute(array(
            ':id_servicio' => $id_servicio
        ));
        $resultados = $consulta->fetchAll();

        if (count($resultados) === 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Servicio no encontrado");
        }

        $consulta = $db->prepare("DELETE from \"Servicio\" where id_servicio = :id_servicio");
        $consulta->execute(array(
            ':id_servicio' => $id_servicio
        ));
        $resultados = $consulta->fetchAll();

        if (count($resultados) === 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Servicio no encontrado.");
        }
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(200, "Servicio borrado");
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
});

/////////////////////////////////////////////////////////
//MUESTRA LA DIRECCION DE UN SERVICIO
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/direccion', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json : '.$tipo_contenido");
    }

    try {

       $consulta = $db->prepare("SELECT * FROM \"Servicio\" WHERE id_servicio= :id");
       $consulta->execute(array(':id'=>$id));

       $resultados= $consulta->fetchAll();

       if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Servicio no encontrado.");
       }

        $consulta = $db->prepare("
        SELECT
            ti.nombre_tipo_vialidad as t,
            nombre_vialidad,
            t1.nombre_tipo_vialidad as t1,
            nombre_vialidad1,
            t2.nombre_tipo_vialidad as t2,
            nombre_vialidad2,
            t3.nombre_tipo_vialidad as t3,
            nombre_vialidad3,
            numero_exterior_km,
            letra_exterior,
            edificio,
            edificio_pis,
            numero_interior,
            letra_interior,	
            corredor_industrial,
            numero_local,
            cp,
            area_geoestadistica,
            manzana,
            tipo_asentamiento_hum
        FROM
            \"Servicio\" AS s,
            \"Tipo_vialidad\" AS ti,
            \"Tipo_vialidad\" AS t1,
            \"Tipo_vialidad\" AS t2,
            \"Tipo_vialidad\" AS t3,
            \"Tipo_asentamiento\" AS asenta
        WHERE
            id_servicio = :id 
            AND s.tipo_vialidad = ti.id_vialidad
            AND	s.tipo_vialidad1 = t1.id_vialidad
            AND s.tipo_vialidad2 = t2.id_vialidad
            AND s.tipo_vialidad3 = t3.id_vialidad
            AND s.tipo_asentamiento_hum = asenta.id_tipo_asentamiento;
        ");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Direccion no encontrada.");
    }
     
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type','application/json');
        $app->render('json/serviciosDireccion.php', array(
            'serviciosDireccion' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml   ');
        $app->render('xml/serviciosDireccion.php', array(
            'serviciosDireccion' => $resultados
        ));
    }
});


/////////////////////////////////////////////////////////
//CREAR DIRECCION DE UN SERVICIO
$app->post('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/direccion', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
     if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml'");
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {

        /* Procesar JSON */
        $servicios = @json_decode($cuerpo_solicitud)->servicios->direccion;
        if (is_null($servicios) || json_last_error() !== JSON_ERROR_NONE || count($servicios) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_nombre_tipo_vialidad0 = $servicios[0]->nombre_tipo_vialidad;
        $nuevo_nombre_vialidad0 = $servicios[0]->nombre_vialidad;
        $nuevo_nombre_tipo_vialidad1 = $servicios[0]->nombre_tipo_vialidad1;
        $nuevo_nombre_vialidad1 = $servicios[0]->nombre_vialidad1;
        $nuevo_nombre_tipo_vialidad2 = $servicios[0]->nombre_tipo_vialidad2;
        $nuevo_nombre_vialidad2 = $servicios[0]->nombre_vialidad2;
        $nuevo_nombre_tipo_vialidad3 = $servicios[0]->nombre_tipo_vialidad3;
        $nuevo_nombre_vialidad3 = $servicios[0]->nombre_vialidad3;
        $nuevo_num_exterior = $servicios[0]->numero_exterior_km;
        $nuevo_letra_exterior = $servicios[0]->letra_exterior;
        $nuevo_edificio = $servicios[0]->edificio;
        $nuevo_edificio_pis = $servicios[0]->edificio_pis;
        $nuevo_numero_inferior = $servicios[0]->numero_interior;
        $nuevo_letra_inferior = $servicios[0]->letra_interior;
        $nuevo_corredor_industrial = $servicios[0]->corredor_industrial;
        $nuevo_numero_local = $servicios[0]->numero_local;
        $nuevo_cp = $servicios[0]->cp;
        $nuevo_area_geoestadistica = $servicios[0]->area_geoestadistica;
        $nuevo_manzana = $servicios[0]->manzana;
        $nuevo_tipo_asentamiento_hum = $servicios[0]->tipo_asentamiento_hum;
    }else if ($tipo_contenido === 'application/xml; charset=UTF-8') {

     /* Procesar XML */
        $servicios = @simplexml_load_string($cuerpo_solicitud)->servicio;
        if (empty($servicios) || count($servicios) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }        
        
        $nuevo_nombre_tipo_vialidad0 = $servicios[0]->nombre_tipo_vialidad;
        $nuevo_nombre_vialidad0 = $servicios[0]->nombre_vialidad;
        $nuevo_nombre_tipo_vialidad1 = $servicios[0]->nombre_tipo_vialidad1;
        $nuevo_nombre_vialidad1 = $servicios[0]->nombre_vialidad1;
        $nuevo_nombre_tipo_vialidad2 = $servicios[0]->nombre_tipo_vialidad2;
        $nuevo_nombre_vialidad2 = $servicios[0]->nombre_vialidad2;
        $nuevo_nombre_tipo_vialidad3 = $servicios[0]->nombre_tipo_vialidad3;
        $nuevo_nombre_vialidad3 = $servicios[0]->nombre_vialidad3;
        $nuevo_num_exterior = $servicios[0]->numero_exterior_km;
        $nuevo_letra_exterior = $servicios[0]->letra_exterior;
        $nuevo_edificio = $servicios[0]->edificio;
        $nuevo_edificio_pis = $servicios[0]->edificio_pis;
        $nuevo_numero_inferior = $servicios[0]->numero_interior;
        $nuevo_letra_inferior = $servicios[0]->letra_interior;
        $nuevo_corredor_industrial = $servicios[0]->corredor_industrial;
        $nuevo_numero_local = $servicios[0]->numero_local;
        $nuevo_cp = $servicios[0]->cp;
        $nuevo_area_geoestadistica = $servicios[0]->area_geoestadistica;
        $nuevo_manzana = $servicios[0]->manzana;
        $nuevo_tipo_asentamiento_hum = $servicios[0]->tipo_asentamiento_hum;
    }

     if (is_null($nuevo_cp) || is_null($nuevo_nombre_tipo_vialidad0 ) || is_null($nuevo_nombre_vialidad0 || is_null($nuevo_num_exterior)) ) {
            $app->halt(406, 'La primera vialidad asi como el nombre, codigo postal y el numero exterior  no pueden ser vacios');
        }
    $consulta = $db->prepare("
   select *   from  \"Servicio\"  where  id_servicio = :id ");
    $consulta->execute(array(
        ':id' => $id
    ));
    $grupos = $consulta->fetchAll();

    if (count($grupos) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio no encontrado.");
    }
    
    $consulta = $db->prepare("
        SELECT
            ti.nombre_tipo_vialidad as t,
            nombre_vialidad,
            t1.nombre_tipo_vialidad as t1,
            nombre_vialidad1,
            t2.nombre_tipo_vialidad as t2,
            nombre_vialidad2,
            t3.nombre_tipo_vialidad as t3,
            nombre_vialidad3,
            numero_exterior_km,
            letra_exterior,
            edificio,
            edificio_pis,
            numero_interior,
            letra_interior,	
            corredor_industrial,
            numero_local,
            cp,
            area_geoestadistica,
            manzana,
            tipo_asentamiento_hum
        FROM
            \"Servicio\" AS s,
            \"Tipo_vialidad\" AS ti,
            \"Tipo_vialidad\" AS t1,
            \"Tipo_vialidad\" AS t2,
            \"Tipo_vialidad\" AS t3,
            \"Tipo_asentamiento\" AS asenta
        WHERE
            id_servicio = :id 
            AND s.tipo_vialidad = ti.id_vialidad
            AND	s.tipo_vialidad1 = t1.id_vialidad
            AND s.tipo_vialidad2 = t2.id_vialidad
            AND s.tipo_vialidad3 = t3.id_vialidad
            AND s.tipo_asentamiento_hum = asenta.id_tipo_asentamiento;
        ");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
        if (count($resultados) !== 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(409, "El servicio ya contiene una direccion");
        }
    

    $consulta0 = $db->prepare("
   select 
   id_vialidad
   from 
    \"Tipo_vialidad\"      
    where 
    nombre_tipo_vialidad like :nombre0;
    
  ");

    $consulta0->execute(array(
        ':nombre0' => $nuevo_nombre_tipo_vialidad0
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Tipo de vialidad invalida");
    }

    $nuevo_tipo_vialidad0 = $resultados[0]['id_vialidad'];

    ////////////////////////////
    $consulta0 = $db->prepare("
    select 
        id_vialidad
    from 
        \"Tipo_vialidad\"      
    where 
        nombre_tipo_vialidad like :nombre1;    
  ");

    $consulta0->execute(array(
        ':nombre1' => $nuevo_nombre_tipo_vialidad1
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Tipo de vialidad1 invalida.");
    }

    $nuevo_tipo_vialidad1 = $resultados[0]['id_vialidad'];

    ////////////////////////////////
    $consulta0 = $db->prepare("
    select 
        id_vialidad
    from 
        \"Tipo_vialidad\"      
    where 
        nombre_tipo_vialidad like :nombre2;    
    ");

    $consulta0->execute(array(
        ':nombre2' => $nuevo_nombre_tipo_vialidad2
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Tipo de vialidad2 invalida");
    }

    $nuevo_tipo_vialidad2 = $resultados[0]['id_vialidad'];


    /////////////////////////////

    $consulta0 = $db->prepare("
   select 
   id_vialidad
   from 
    \"Tipo_vialidad\"      
    where 
    nombre_tipo_vialidad like :nombre3;    
  ");

    $consulta0->execute(array(
        ':nombre3' => $nuevo_nombre_tipo_vialidad3
    ));

    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Tipo de vialidad3 invalida");
    }

    $nuevo_tipo_vialidad3 = $resultados[0]['id_vialidad'];

    ////////////////////////////

    $consulta = $db->prepare("
    UPDATE
        \"Servicio\" AS s
    SET  
        tipo_vialidad = :tipo_vialidad0,
        nombre_vialidad = :nombre_vialidad0,
        tipo_vialidad1 = :tipo_vialidad1,
        nombre_vialidad1 = :nombre_vialidad1,
        tipo_vialidad2 = :tipo_vialidad2,
        nombre_vialidad2 = :nombre_vialidad2,
        tipo_vialidad3 = :tipo_vialidad3,
        nombre_vialidad3 = :nombre_vialidad3,
        numero_exterior_km = :numero_exterior_km,
        letra_exterior = :letra_exterior,
        edificio =:edificio,
        edificio_pis = :edificio_pis,
        numero_interior = :numero_interior, 
        letra_interior = :letra_interior,	
        corredor_industrial = :corredor_industrial,
        numero_local = :numero_local,
        cp = :cp,
        area_geoestadistica = :area_geoestadistica,
        manzana = :manzana,
        tipo_asentamiento_hum = :tipo_asentamiento_hum
    WHERE
    	id_servicio = :id
  ");
    $consulta->execute(array(
        ':tipo_vialidad0' => $nuevo_tipo_vialidad0,
        'nombre_vialidad0' => $nuevo_nombre_vialidad0,
        ':tipo_vialidad1' => $nuevo_tipo_vialidad1,
        ':nombre_vialidad1' => $nuevo_nombre_vialidad1,
        ':tipo_vialidad2' => $nuevo_tipo_vialidad2,
        ':nombre_vialidad2' => $nuevo_nombre_vialidad2,
        ':tipo_vialidad3' => $nuevo_tipo_vialidad3,
        ':nombre_vialidad3' => $nuevo_nombre_vialidad3,
        ':numero_exterior_km' => $nuevo_num_exterior,
        ':letra_exterior' => $nuevo_letra_exterior,
        ':edificio' => $nuevo_edificio,
        ':edificio_pis' => $nuevo_edificio_pis,
        ':numero_interior' => $nuevo_numero_inferior,
        ':letra_interior' => $nuevo_letra_inferior,
        ':corredor_industrial' => $nuevo_corredor_industrial,
        ':numero_local' => $nuevo_numero_local,
        ':cp' => $nuevo_cp,
        ':area_geoestadistica' => $nuevo_area_geoestadistica,
        ':manzana' => $nuevo_manzana,
        'tipo_asentamiento_hum' => $nuevo_tipo_asentamiento_hum,
        ':id' => $id
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "servicio con direcccion ya existente.");
    }

    $app->halt(201, 'El servicio a sido creado');
});

/////////////////////////////////////////////////////////
//ACTUALIZAR DIRECCION DE UN SERVICIO
$app->put('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/direccion', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
     if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json' = " . $tipo_contenido);
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {

        /* Procesar JSON */
        $servicios = @json_decode($cuerpo_solicitud)->servicios->direccion;
        if (is_null($servicios) || json_last_error() !== JSON_ERROR_NONE || count($servicios) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_nombre_tipo_vialidad0 = $servicios[0]->nombre_tipo_vialidad;
        $nuevo_nombre_vialidad0 = $servicios[0]->nombre_vialidad;
        $nuevo_nombre_tipo_vialidad1 = $servicios[0]->nombre_tipo_vialidad1;
        $nuevo_nombre_vialidad1 = $servicios[0]->nombre_vialidad1;
        $nuevo_nombre_tipo_vialidad2 = $servicios[0]->nombre_tipo_vialidad2;
        $nuevo_nombre_vialidad2 = $servicios[0]->nombre_vialidad2;
        $nuevo_nombre_tipo_vialidad3 = $servicios[0]->nombre_tipo_vialidad3;
        $nuevo_nombre_vialidad3 = $servicios[0]->nombre_vialidad3;
        $nuevo_num_exterior = $servicios[0]->numero_exterior_km;
        $nuevo_letra_exterior = $servicios[0]->letra_exterior;
        $nuevo_edificio = $servicios[0]->edificio;
        $nuevo_edificio_pis = $servicios[0]->edificio_pis;
        $nuevo_numero_inferior = $servicios[0]->numero_interior;
        $nuevo_letra_inferior = $servicios[0]->letra_interior;
        $nuevo_corredor_industrial = $servicios[0]->corredor_industrial;
        $nuevo_numero_local = $servicios[0]->numero_local;
        $nuevo_cp = $servicios[0]->cp;
        $nuevo_area_geoestadistica = $servicios[0]->area_geoestadistica;
        $nuevo_manzana = $servicios[0]->manzana;
        $nuevo_tipo_asentamiento_hum = $servicios[0]->tipo_asentamiento_hum;
    }

    $consulta = $db->prepare("
   select *   from  \"Servicio\"  where  id_servicio = :id ");
    $consulta->execute(array(
        ':id' => $id
    ));
    $grupos = $consulta->fetchAll();

    if (count($grupos) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio no encontrado.");
    }

    $consulta0 = $db->prepare("
   select 
   id_vialidad
   from 
    \"Tipo_vialidad\"      
    where 
    nombre_tipo_vialidad like :nombre0;
    
  ");

    $consulta0->execute(array(
        ':nombre0' => $nuevo_nombre_tipo_vialidad0
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio con direcccion ya existente.");
    }

    $nuevo_tipo_vialidad0 = $resultados[0]['id_vialidad'];

    ////////////////////////////
    $consulta0 = $db->prepare("
    select 
        id_vialidad
    from 
        \"Tipo_vialidad\"      
    where 
        nombre_tipo_vialidad like :nombre1;    
  ");

    $consulta0->execute(array(
        ':nombre1' => $nuevo_nombre_tipo_vialidad1
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio con direcccion ya existente.");
    }

    $nuevo_tipo_vialidad1 = $resultados[0]['id_vialidad'];

    ////////////////////////////////
    $consulta0 = $db->prepare("
    select 
        id_vialidad
    from 
        \"Tipo_vialidad\"      
    where 
        nombre_tipo_vialidad like :nombre2;    
    ");

    $consulta0->execute(array(
        ':nombre2' => $nuevo_nombre_tipo_vialidad2
    ));


    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio con direcccion ya existente.");
    }

    $nuevo_tipo_vialidad2 = $resultados[0]['id_vialidad'];


    /////////////////////////////

    $consulta0 = $db->prepare("
   select 
   id_vialidad
   from 
    \"Tipo_vialidad\"      
    where 
    nombre_tipo_vialidad like :nombre3;    
  ");

    $consulta0->execute(array(
        ':nombre3' => $nuevo_nombre_tipo_vialidad3
    ));

    $resultados = $consulta0->fetchAll();
    if ($consulta0->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio con direcccion ya existente.");
    }

    $nuevo_tipo_vialidad3 = $resultados[0]['id_vialidad'];

    ////////////////////////////

    $consulta = $db->prepare("
    UPDATE
        \"Servicio\" AS s
    SET  
        tipo_vialidad = :tipo_vialidad0,
        nombre_vialidad = :nombre_vialidad0,
        tipo_vialidad1 = :tipo_vialidad1,
        nombre_vialidad1 = :nombre_vialidad1,
        tipo_vialidad2 = :tipo_vialidad2,
        nombre_vialidad2 = :nombre_vialidad2,
        tipo_vialidad3 = :tipo_vialidad3,
        nombre_vialidad3 = :nombre_vialidad3,
        numero_exterior_km = :numero_exterior_km,
        letra_exterior = :letra_exterior,
        edificio =:edificio,
        edificio_pis = :edificio_pis,
        numero_interior = :numero_interior, 
        letra_interior = :letra_interior,	
        corredor_industrial = :corredor_industrial,
        numero_local = :numero_local,
        cp = :cp,
        area_geoestadistica = :area_geoestadistica,
        manzana = :manzana,
        tipo_asentamiento_hum = :tipo_asentamiento_hum
    WHERE
    	id_servicio = :id
  ");
    $consulta->execute(array(
        ':tipo_vialidad0' => $nuevo_tipo_vialidad0,
        'nombre_vialidad0' => $nuevo_nombre_vialidad0,
        ':tipo_vialidad1' => $nuevo_tipo_vialidad1,
        ':nombre_vialidad1' => $nuevo_nombre_vialidad1,
        ':tipo_vialidad2' => $nuevo_tipo_vialidad2,
        ':nombre_vialidad2' => $nuevo_nombre_vialidad2,
        ':tipo_vialidad3' => $nuevo_tipo_vialidad3,
        ':nombre_vialidad3' => $nuevo_nombre_vialidad3,
        ':numero_exterior_km' => $nuevo_num_exterior,
        ':letra_exterior' => $nuevo_letra_exterior,
        ':edificio' => $nuevo_edificio,
        ':edificio_pis' => $nuevo_edificio_pis,
        ':numero_interior' => $nuevo_numero_inferior,
        ':letra_interior' => $nuevo_letra_inferior,
        ':corredor_industrial' => $nuevo_corredor_industrial,
        ':numero_local' => $nuevo_numero_local,
        ':cp' => $nuevo_cp,
        ':area_geoestadistica' => $nuevo_area_geoestadistica,
        ':manzana' => $nuevo_manzana,
        'tipo_asentamiento_hum' => $nuevo_tipo_asentamiento_hum,
        ':id' => $id
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "servicio con direcccion ya existente.");
    }

    $app->halt(200, 'SERVICIO ha sido actualizado.');
});

/////////////////////////////////////////////////////////
//DELETE DIRECCION
$app->delete('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/direccion', 
    function($id_entidad, $id_municipio, $id_localidad, $id) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json'");
    }
    try {

          $consulta = $db->prepare("
        SELECT
            ti.nombre_tipo_vialidad as t,
            nombre_vialidad,
            t1.nombre_tipo_vialidad as t1,
            nombre_vialidad1,
            t2.nombre_tipo_vialidad as t2,
            nombre_vialidad2,
            t3.nombre_tipo_vialidad as t3,
            nombre_vialidad3,
            numero_exterior_km,
            letra_exterior,
            edificio,
            edificio_pis,
            numero_interior,
            letra_interior,	
            corredor_industrial,
            numero_local,
            cp,
            area_geoestadistica,
            manzana,
            tipo_asentamiento_hum
        FROM
            \"Servicio\" AS s,
            \"Tipo_vialidad\" AS ti,
            \"Tipo_vialidad\" AS t1,
            \"Tipo_vialidad\" AS t2,
            \"Tipo_vialidad\" AS t3,
            \"Tipo_asentamiento\" AS asenta
        WHERE
            id_servicio = :id 
            AND s.tipo_vialidad = ti.id_vialidad
            AND	s.tipo_vialidad1 = t1.id_vialidad
            AND s.tipo_vialidad2 = t2.id_vialidad
            AND s.tipo_vialidad3 = t3.id_vialidad
            AND s.tipo_asentamiento_hum = asenta.id_tipo_asentamiento;
        ");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();

        if (count($resultados) === 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Servicio no encontrado");
        }

        $consulta = $db->prepare("
            UPDATE
                \"Servicio\" AS s
            SET  
                tipo_vialidad = :tipo_vialidad0,
                nombre_vialidad = :nombre_vialidad0,
                tipo_vialidad1 = :tipo_vialidad1,
                nombre_vialidad1 = :nombre_vialidad1,
                tipo_vialidad2 = :tipo_vialidad2,
                nombre_vialidad2 = :nombre_vialidad2,
                tipo_vialidad3 = :tipo_vialidad3,
                nombre_vialidad3 = :nombre_vialidad3,
                numero_exterior_km = :numero_exterior_km,
                letra_exterior = :letra_exterior,
                edificio =:edificio,
                edificio_pis = :edificio_pis,
                numero_interior = :numero_interior, 
                letra_interior = :letra_interior,	
                corredor_industrial = :corredor_industrial,
                numero_local = :numero_local,
                cp = :cp,
                area_geoestadistica = :area_geoestadistica,
                manzana = :manzana,
                tipo_asentamiento_hum = :tipo_asentamiento_hum
            WHERE
            	id_servicio = :id
          ");
        $consulta->execute(array(
            ':tipo_vialidad0' => NULL,
            'nombre_vialidad0' => NULL,
            ':tipo_vialidad1' => NULL,
            ':nombre_vialidad1' => NULL,
            ':tipo_vialidad2' => NULL,
            ':nombre_vialidad2' => NULL,
            ':tipo_vialidad3' => NULL,
            ':nombre_vialidad3' => NULL,
            ':numero_exterior_km' => NULL,
            ':letra_exterior' => NULL,
            ':edificio' => NULL,
            ':edificio_pis' => NULL,
            ':numero_interior' => NULL,
            ':letra_interior' => NULL,
            ':corredor_industrial' => NULL,
            ':numero_local' => NULL,
            ':cp' => NULL,
            ':area_geoestadistica' => NULL,
            ':manzana' => NULL,
            'tipo_asentamiento_hum' => NULL,
            ':id' => $id
        ));

        $resultados = $consulta->fetchAll();

        if (count($resultados) === 0) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(404, "Servicio no encontrado.");
        }
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(200, "Direccion  borrada");
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
});


/////////////////////////////////////////////////////////
//MUESTRA LOS CONTACTOS
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/contacto', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {

      if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml'");
    }

    try {
        $consulta = $db->prepare("
        select 
            numero_telefono,
            correo_electronico, 
            sitio_internet 
        FROM 
            \"Servicio\" 
        where 
            id_servicio = :id");
        
        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Entidad no encontrada.");
    }

    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/contacto.php', array(
            'contactos' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
    $app->response->headers->set('Content-Type', 'application/xml');
    $app->render('xml/contacto.php', array(
        'contactos' => $resultados
    ));
    }
});

/////////////////////////////////////////////////////////
//UPDATE CONTACTOS
$app->put('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/contacto', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
    
     if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }

    
    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json' = " . $tipo_contenido);
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {

        /* Procesar JSON */
        $servicios = @json_decode($cuerpo_solicitud)->servicios->contacto;
        if (is_null($servicios) || json_last_error() !== JSON_ERROR_NONE || count($servicios) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_numero_telefono = $servicios[0]->numero_telefono;
        $nuevo_correo_electronico = $servicios[0]->correo_electronico;
        $nuevo_sitio_internet = $servicios[0]->sitio_internet;
    }else if ($tipo_contenido === 'application/xml; charset=UTF-8') {
        /* Procesar XML */
        $servicios = @simplexml_load_string($cuerpo_solicitud)->contacto;
        if (empty($servicios) || count($servicios) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        }   
        $nuevo_numero_telefono = $servicios[0]->numero_telefono;
        $nuevo_correo_electronico = $servicios[0]->correo_electronico;
        $nuevo_sitio_internet = $servicios[0]->sitio_internet;
    }


    $consulta = $db->prepare("
   select numero_telefono, correo_electronico, sitio_internet
    from \"Servicio\"
    where id_servicio = :id
  ");
    $consulta->execute(array(
        ':id' => $id
    ));
    $grupos = $consulta->fetchAll();



    if (count($grupos) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio no encontrado.");
    }


    $consulta = $db->prepare("
     update
    \"Servicio\"
     set numero_telefono = :numtel,
 	 correo_electronico = :correoelec,
     sitio_internet = :web
    where id_servicio = :id
  ");
    $consulta->execute(array(
        ':numtel' => $nuevo_numero_telefono,
        ':correoelec' => $nuevo_correo_electronico,
        ':web' => $nuevo_sitio_internet,
        ':id' => $id
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "servicio con numero_telefono ya existente.");
        $app->halt(400, "servicio con correo_electrónico ya existente.");
        $app->halt(400, "servicio con sitio_internet ya existente.");
    }

    $app->halt(200, 'Contacto se ha sido actualizado.');
});

/////////////////////////////////////////////////////////
//MUESTRA COORDENADAS DE UN SERVICIO :D
$app->get('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/coordenadas', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json : '.$tipo_contenido");
    }

    try {
        $consulta = $db->prepare("select latitud, longitud from \"Servicio\" where id_servicio = :id");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "Servicio no encontrado.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/serviciosCoordenadas.php', array(
            'serviciosCoordenadas' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/serviciosCoordenadas.php', array(
            'serviciosCoordenadas' => $resultados
        ));
    }
    
});

/////////////////////////////////////////////////////////
//ACTUALIZAR COORDENADAS
$app->put('/entidades/:id_entidad/municipios/:id_municipio/localidades/:id_localidad/servicios/:id/coordenadas', 
    function($id_entidad, $id_municipio, $id_localidad,$id) use ($db, $app) {
    
    if (!is_numeric($id_entidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de entidad no válido");
    }
    if (!is_numeric($id_municipio)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de municipio no válido");
    }
    if (!is_numeric($id_localidad)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de localidad no válido");
    }    
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "ID de servicio no válido");
    }

    $tipo_contenido = $app->request->headers->get('Content-Type');

    if ($tipo_contenido !== 'application/json; charset=UTF-8' && $tipo_contenido !== 'application/xml; charset=UTF-8') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json' = " . $tipo_contenido);
    }

    $cuerpo_solicitud = $app->request->getBody();
    if ($tipo_contenido === 'application/json; charset=UTF-8') {

        /* Procesar JSON */
        $servicios = @json_decode($cuerpo_solicitud)->servicios->coordenadas;
        if (is_null($servicios) || json_last_error() !== JSON_ERROR_NONE || count($servicios) !== 1) {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->halt(415, "JSON no valido.");
        }
        $nuevo_latitud = $servicios[0]->latitud;
        $nuevo_longitud = $servicios[0]->longitud;
    }else if ($tipo_contenido === 'application/xml; charset=UTF-8') {
        /* Procesar XML */
        $servicios = @simplexml_load_string($cuerpo_solicitud)->coordenadas;
        if (empty($servicios) || count($servicios) !== 1) {
          $app->response->headers->set('Content-Type', 'text/plain');
          $app->halt(415, "XML no valido.");
        } 
        $nuevo_latitud = $servicios[0]->latitud;
        $nuevo_longitud = $servicios[0]->longitud;
    }

    $consulta = $db->prepare("
   select latitud, longitud
    from \"Servicio\"
    where id_servicio = :id
  ");
    $consulta->execute(array(
        ':id' => $id
    ));
    $grupos = $consulta->fetchAll();

    if (count($grupos) === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "servicio no encontrado.");
    }


    $consulta = $db->prepare("
     update
    \"Servicio\"
    set latitud = :lat,
 	 longitud = :long
    where id_servicio = :id
  ");
    $consulta->execute(array(
        ':lat' => $nuevo_latitud,
        ':long' => $nuevo_longitud,
        ':id' => $id
    ));

    if ($consulta->rowCount() === 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "Servicio con cordenadas ya existente.");
        
    }

    $app->halt(200, 'Coordenadas han sido actualizadas.');
    
});

/////////////////////////////////////////////////////////
//MUESTRA LOS ESTRATOS PERMITIDOS
$app->get('/estratos', function() use ($db, $app) {
    
    $tipo_contenido = $app->request->headers->get('Accept');

    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(415, "Content-Type validos: 'application/json', 'application/xml");
    }

    try {

        $consulta = $db->prepare("select * from \"Descripcion_estrato_personal\"");
        $consulta->execute();
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/estratos.php', array(
            'estrato_personal' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/estratos.php', array(
            'estrato_personal' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//MUESTRA UN ESTRATO PERMITIDO
$app->get('/estratos/:id', function($id) use ($db, $app) {
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "id estrato no válido");
    }


    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml");
    }

    try {
        $consulta = $db->prepare("
        select 
            * 
        from 
            \"Descripcion_estrato_personal\" 
        where 
            id_descripcion = :id");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "actividad no encontrada.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/estratos.php', array(
            'estrato_personal' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/estratos.php', array(
            'estrato_personal' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//GET VIALIDADES
$app->get('/vialidades', function() use ($db, $app) {


    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json', 'application/xml'");
    }
    try {

        $consulta = $db->prepare("select * from \"Tipo_vialidad\"");

        $consulta->execute();
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/vialidades.php', array(
            'vialidades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/vialidades.php', array(
            'vialidades' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//GET ASENTAMIENTOS
$app->get('/asentamientos', function() use ($db, $app) {
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json : '.$tipo_contenido");
    }
    try {

        $consulta = $db->prepare("select * from \"Tipo_asentamiento\"");
        $consulta->execute();
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/asentamiento.php', array(
            'asentamiento' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/asentamiento.php', array(
            'asentamiento' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//MUESTRA LAS ACTIVIDADES DISPONIBLES
$app->get('/actividades', function() use ($db, $app) {
    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json'");
    }
    try {

        $consulta = $db->prepare("select * from \"Actividad\"");
        $consulta->execute();
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {

        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/actividades.php', array(
            'actividades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/actividades.php', array(
            'actividades' => $resultados
        ));
    }
});

/////////////////////////////////////////////////////////
//MUESTRA LA ACTIVIDADES
$app->get('/actividades/:id', function($id) use ($db, $app) {
    if (!is_numeric($id)) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(400, "id de Actividad no válido");
    }


    $tipo_contenido = $app->request->headers->get('Accept');
    if ($tipo_contenido !== 'application/json' && $tipo_contenido !== 'application/xml') {
        $app->response->headers->set('Content-type', 'text/plain');
        $app->halt(406, "Accepts validos 'application/json : '.$tipo_contenido");
    }

    try {
        $consulta = $db->prepare("select * from \"Actividad\" where codigo_actividad = :id");

        $consulta->execute(array(
            ':id' => $id
        ));
        $resultados = $consulta->fetchAll();
    } catch (PDOException $e) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(500, "Error");
    }

    if (count($resultados) == 0) {
        $app->response->headers->set('Content-Type', 'text/plain');
        $app->halt(404, "actividad no encontrada.");
    }
    if ($tipo_contenido === 'application/json') {
        $app->response->headers->set('Content-Type', 'application/json');
        $app->render('json/actividades.php', array(
            'actividades' => $resultados
        ));
    }else if ($tipo_contenido === 'application/xml') {
        $app->response->headers->set('Content-Type', 'application/xml');
        $app->render('xml/actividades.php', array(
            'actividades' => $resultados
        ));
    }
});




$app->run();
